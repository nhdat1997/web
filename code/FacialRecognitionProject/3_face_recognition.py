import cv2
import sqlite3
import numpy as np
import os

recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read('lib/trainer/trainer.yml')
cascadePath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath)
font = cv2.FONT_HERSHEY_SIMPLEX
fontface = cv2.FONT_HERSHEY_SIMPLEX
fontscale = 0.5
fontcolor = (255, 255, 0)
id = 0

cam = cv2.VideoCapture('http://192.168.0.102:8080/mjpeg')
cam.set(3, 640)  # set video widht
cam.set(4, 480)  # set video height
# Define min window size to be recognized as a face
minW = 0.1 * cam.get(3)
minH = 0.1 * cam.get(4)

# get data from sqlite by ID
def getProfile(id):
    conn = sqlite3.connect("db.sqlite3")
    cmd = "SELECT * FROM webstream_people WHERE id=" + str(id)
    cursor = conn.execute(cmd)
    profile = None
    for row in cursor:
        profile = row
    conn.close()
    return profile

while True:
    ret, img = cam.read()
    #img = cv2.flip(img, -1)  # Flip vertically
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.2,
        minNeighbors=5,
        minSize=(int(minW), int(minH))
    )
    for (x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
        id, confidence = recognizer.predict(gray[y:y + h, x:x + w])
        profile = getProfile(id)
        # Check if confidence is less them 100 ==> "0" is perfect match
        if profile != None:
            if 75 > confidence > 20:
                # cv2.PutText(cv2.fromarray(img),str(id),(x+y+h),font,(0,0,255),2);
                #cv2.putText(img, "ID: " + str(profile[1]), (x, y + h + 30), fontface, fontscale, fontcolor, 2)
                cv2.putText(img, "Name: " + str(profile[1]), (x, y + h + 30), fontface, fontscale, fontcolor, 2)
                cv2.putText(img, "Age: " + str(profile[2]), (x, y + h + 60), fontface, fontscale, fontcolor, 2)
                cv2.putText(img, "Gender: " + str(profile[3]), (x, y + h + 90), fontface, fontscale, fontcolor, 2)
                confidence = "  {0}%".format(round(100 - confidence))
            else:
                id= "Unknow"
                confidence = "  {0}%".format(round(100 - confidence))

        cv2.putText(img, str(id), (x + 5, y - 5), font, fontscale, fontcolor, 1, 2)
        cv2.putText(img, str(confidence), (x + 5, y + h - 5), font, fontscale, fontcolor, 1, 1)

    cv2.imshow('camera', img)
    k = cv2.waitKey(10) & 0xff  # Press 'ESC' for exiting video
    if k == 27:
        break
# Do a bit of cleanup
print("\n [INFO] Exiting Program and cleanup stuff")
cam.release()
cv2.destroyAllWindows()