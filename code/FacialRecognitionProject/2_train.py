import cv2
import numpy as np
from PIL import Image
import os

# Path for face image database
path = 'lib/dataset'
recognizer = cv2.face.LBPHFaceRecognizer_create()
detector = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")


# function to get the images and label data
def getImagesAndLabels(path):
    FJoin = os.path.join
    def GetFiles(path):
        file_list, dir_list = [], []
        for dir, subdirs, files in os.walk(path):
            file_list.extend([FJoin(dir, f) for f in files])
            dir_list.extend([FJoin(dir, d) for d in subdirs])
        file_list = filter(lambda x: not os.path.islink(x), file_list)
        return file_list
    files = GetFiles(os.path.expanduser(path))

    imagePaths = [os.path.join(file) for file in files]
    faceSamples = []
    ids = []
    for imagePath in imagePaths:
        PIL_img = Image.open(imagePath).convert('L')  # convert it to grayscale
        img_numpy = np.array(PIL_img, 'uint8')
        id = int(os.path.split(imagePath)[-1].split(".")[1])
        faces = detector.detectMultiScale(img_numpy)
        for (x, y, w, h) in faces:
            faceSamples.append(img_numpy[y:y + h, x:x + w])
            ids.append(id)
    return faceSamples, ids


print("\n [INFO] Training faces. It will take a few seconds. Wait ...")
faces, ids = getImagesAndLabels(path)
recognizer.train(faces, np.array(ids))
# Save the model into trainer/trainer.yml
recognizer.write('lib/trainer/trainer.yml')  # recognizer.save() worked on Mac, but not on Pi
# Print the numer of faces trained and end program
print("\n [INFO] {0} faces trained. Exiting Program".format(len(np.unique(ids))))
