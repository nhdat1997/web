import sys
import time
import cv2

sys.path.insert(0, '../imagezmq')  # imagezmq.py is in ../imagezmq
import imagezmq

sender = imagezmq.ImageSender(connect_to='tcp://*:8080', REQ_REP=False)
cam = cv2.VideoCapture(0)
image_window_name = 'From Sender'
time.sleep(.0)  # allow camera sensor to warm up

while True:
    ret, img = cam.read()
    #cv2.imshow('image', img)
    sender.send_image(image_window_name, img)
    time.sleep(1)
    k = cv2.waitKey(100) & 0xff  # Press 'ESC' for exiting videoqq
    if k == 27:
        break
cam.release()
cv2.destroyAllWindows()