from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    path('FaceRec/', views.VideoFaceRec, name='FaceRec'),
    path('NewFaceRec/', views.NewFaceRec, name='NewFaceRec'),
    url(r'^quanly/$', views.quanly, name='quanly'),
    url(r'^guest/$', views.guest, name='guest'),
    url(r'', views.home, name='home'),
]