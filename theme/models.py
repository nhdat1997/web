from django.db import models


class People(models.Model):
    GT = (('M','Men'), ('W','Woman'))
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=30)
    age = models.IntegerField()
    gt = models.CharField(max_length=5, choices=GT)
    birth = models.DateField()
    sdt = models.IntegerField()
    email = models.EmailField()
    addr = models.TextField(max_length=100)
