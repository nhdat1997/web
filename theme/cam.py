import sqlite3
import cv2
from lib.imagezmq import imagezmq




def StreamTcp():
    image_hub = imagezmq.ImageHub(open_port='tcp://192.168.0.100:8080', REQ_REP=False)
    face_detector = cv2.CascadeClassifier('lib/haarcascade/haarcascade_frontalface_default.xml')
    while True:  # press Ctrl-C to stop image display program
        cam_name, cam = image_hub.recv_image()
        gray = cv2.cvtColor(cam, cv2.COLOR_BGR2GRAY)
        faces = face_detector.detectMultiScale(gray, 1.3, 5)
        for (x, y, w, h) in faces:
            cv2.rectangle(cam, (x, y), (x + w, y + h), (255, 0, 0), 2)
        cv2.imwrite('demo.jpg', cam)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + open('demo.jpg', 'rb').read() + b'\r\n')


def FaceRecognition():
    recognizer = cv2.face.LBPHFaceRecognizer_create()
    recognizer.read('lib/trainer/trainer.yml')
    faceCascade = cv2.CascadeClassifier("lib/haarcascade/haarcascade_frontalface_default.xml")
    fontface = cv2.FONT_HERSHEY_SIMPLEX
    fontscale = 0.5
    fontcolor = (255, 255, 0)
    cam = cv2.VideoCapture(0)
    cam.set(3, 640)  # set video widht
    cam.set(4, 480)  # set video height
    # Define min window size to be recognized as a face
    minW = 0.1 * cam.get(3)
    minH = 0.1 * cam.get(4)
    # get data from sqlite by ID
    def getProfile(id):
        conn = sqlite3.connect("db.sqlite3")
        cmd = 'SELECT * FROM theme_people WHERE id=' + str(id)
        cursor = conn.execute(cmd)
        prof = None
        for row in cursor:
            prof = row
        conn.close()
        return prof
    while True:
        ret, img = cam.read()
        # img = cv2.flip(img, -1)  # Flip vertically
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.2,
            minNeighbors=5,
            minSize=(int(minW), int(minH))
        )
        for (x, y, w, h) in faces:
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
            id, confidence = recognizer.predict(gray[y:y + h, x:x + w])
            profile = getProfile(id)
            # Check if confidence is less them 100 ==> "0" is perfect match
            if profile is not None:
                if 75 > confidence > 20:
                    cv2.putText(img, "Name: " + str(profile[1]), (x, y + h + 30), fontface, fontscale, fontcolor, 2)
                    cv2.putText(img, "Age: " + str(profile[2]), (x, y + h + 60), fontface, fontscale, fontcolor, 2)
                    cv2.putText(img, "Gender: " + str(profile[3]), (x, y + h + 90), fontface, fontscale, fontcolor, 2)
                    confidence = "  {0}%".format(round(100 - confidence))
                else:
                    id = "Who is this?"
                    confidence = "{0}%".format(round(100 - confidence))
            cv2.imwrite('lib/face/'+str(id)+'.jpg',img[y:y + h, x:x + w])
            cv2.putText(img, str(id), (x + 5, y - 5), fontface, fontscale, fontcolor, 1, 2)
            cv2.putText(img, str(confidence), (x + 5, y + h - 5), fontface, fontscale, fontcolor, 1, 1)

        cv2.imwrite('cam.jpg', img)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + open('cam.jpg', 'rb').read() + b'\r\n')

def NewFaceRecognition():
    recognizer = cv2.face.LBPHFaceRecognizer_create()
    recognizer.read('lib/trainer/trainer.yml')
    faceCascade = cv2.CascadeClassifier("lib/haarcascade/haarcascade_frontalface_default.xml")
    fontface = cv2.FONT_HERSHEY_SIMPLEX
    fontscale = 0.5
    fontcolor = (255, 255, 0)
    cam = cv2.VideoCapture('http://192.168.0.103:8080/mjpeg')
    cam.set(3, 640)  # set video widht
    cam.set(4, 480)  # set video height
    # Define min window size to be recognized as a face
    minW = 0.1 * cam.get(3)
    minH = 0.1 * cam.get(4)

    # get data from sqlite by ID
    def getProfile(id):
        conn = sqlite3.connect("db.sqlite3")
        cmd = 'SELECT * FROM pages_people WHERE id=' + str(id)
        cursor = conn.execute(cmd)
        prof = None
        for row in cursor:
            prof = row
        conn.close()
        return prof

    count = 0
    while True:
        ret, img = cam.read()
        # img = cv2.flip(img, -1)  # Flip vertically
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.2,
            minNeighbors=5,
            minSize=(int(minW), int(minH))
        )
        for (x, y, w, h) in faces:
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
            id, confidence = recognizer.predict(gray[y:y + h, x:x + w])
            profile = getProfile(id)
            # Check if confidence is less them 100 ==> "0" is perfect match
            if profile is not None:
                if 75 > confidence > 20:
                    cv2.putText(img, "Name: " + str(profile[1]), (x, y + h + 30), fontface, fontscale, fontcolor, 2)
                    cv2.putText(img, "Age: " + str(profile[2]), (x, y + h + 60), fontface, fontscale, fontcolor, 2)
                    cv2.putText(img, "Gender: " + str(profile[3]), (x, y + h + 90), fontface, fontscale, fontcolor, 2)
                    confidence = "  {0}%".format(round(100 - confidence))
                else:
                    id = "Unknow"
                    confidence = "{0}%".format(round(100 - confidence))
                    count += 1
                    cv2.imwrite('lib/Find/Un.' + str(count) + ".jpg", img[y:y + h, x:x + w])
            cv2.imwrite('lib/face/' + str(id) + '.jpg', img[y:y + h, x:x + w])
            cv2.putText(img, str(id), (x + 5, y - 5), fontface, fontscale, fontcolor, 1, 2)
            cv2.putText(img, str(confidence), (x + 5, y + h - 5), fontface, fontscale, fontcolor, 1, 1)

        cv2.imwrite('demo.jpg', img)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + open('demo.jpg', 'rb').read() + b'\r\n')
