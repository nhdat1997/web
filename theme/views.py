from django.http import StreamingHttpResponse
from django.template.response import TemplateResponse
from django.shortcuts import render
# Create your views here.
from theme.cam import StreamTcp, FaceRecognition, NewFaceRecognition


def homepage(request):
    return TemplateResponse(request, 'base.html')


def guest(request):
    return render(request, 'guest.html', {})


def home(request):
    return render(request, 'home.html', {})


def quanly(request):
    return render(request, 'quanly.html', {})


def VideoTcp(request):
    return StreamingHttpResponse(StreamTcp(), content_type='multipart/x-mixed-replace; boundary=frame')


def VideoFaceRec(request):
    return StreamingHttpResponse(FaceRecognition(), content_type='multipart/x-mixed-replace; boundary=frame')


def NewFaceRec(request):
    return StreamingHttpResponse(NewFaceRecognition(), content_type='multipart/x-mixed-replace; boundary=frame')