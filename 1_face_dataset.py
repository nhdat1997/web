import cv2
import sqlite3
import os
import numpy as np
from PIL import Image

def insertOrUpdate(id, name):
    conn = sqlite3.connect("db.sqlite3")
    os.chdir('lib/dataset')
    try:
        print("connected")
        cmd = "INSERT INTO stream_people(id, name) VALUES (?,?);"
        data = (id, name)
        conn.execute(cmd, data)
        os.mkdir(name + '_' +id)
        conn.commit()
        conn.close()
    except sqlite3.Error:
        cmd = "SELECT * FROM stream_people WHERE id=" + str(id)
        cursor = conn.execute(cmd)
        for row in cursor:
            print(row)
        if str(row[0]) == str(id):
            cmd = "update stream_people set name = '{}'".format(str(name)) + " where id = " + str(id)
            conn.execute(cmd)
            print("updated id")
            conn.commit()
            conn.close()
    finally:
        if conn:
            conn.close()
            print("The SQLite connection is closed")


cam = cv2.VideoCapture(0)
cam.set(3, 640)  # set video width
cam.set(4, 480)  # set video height
face_detector = cv2.CascadeClassifier('lib/haarcascade/haarcascade_frontalface_default.xml')
# For each person, enter one numeric face id
id = input('\nenter user id end press <return> ==>  ')
name = input('\nenter username end press <return> ==>  ')
insertOrUpdate(id, name)
print("\n [INFO] Initializing face capture. Look the camera and wait ...")
count = 0

while True:
    ret, img = cam.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_detector.detectMultiScale(gray, 1.3, 5)
    for (x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
        count += 1
        # Save the captured image into the datasets folder
        cv2.imwrite("{}_{}/".format(name, id) + str(name) + '.' + str(id) + '.' + str(count) + ".jpg", gray[y:y + h, x:x + w])
    cv2.imshow('image', img)
    k = cv2.waitKey(100) & 0xff  # Press 'ESC' for exiting videoqq
    if k == 27:
        break
    elif count >= 20:  # Take 30 face sample and stop video
        break
# Do a bit of cleanup
print("\n [INFO] Exiting Program and cleanup stuff")
cam.release()
cv2.destroyAllWindows()